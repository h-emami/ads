import React from 'react'
import './styles.css'

const Spinner = () => {
	return (
		<div className='Spinner'>
			<div className='loader'></div>
		</div>
	)
}

export default Spinner
