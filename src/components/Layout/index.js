import React from 'react'
import './styles.css'
import Header from '../Header'
import Footer from '../Footer'
import Spinner from '../Spinner'
import { connect } from 'react-redux'
import Error from '../Error'

const Layout = ({ children, loading, error }) => {
	return (
		<div className='Layout'>
			<Header />
			{loading ? <Spinner /> : error ? <Error error={error} /> : children}
			<Footer />
		</div>
	)
}

const mapStateToProps = ({ ads }) => ({
	loading: ads.isLoading,
	error: ads.error
})

export default connect(mapStateToProps)(Layout)
