import React from 'react'
import './styles.css'

const Ad = ({
	fileNumber,
	location,
	area,
	buildDate,
	costPerMeter,
	totalCost
}) => {
	const getBuildDate = buildDate => buildDate.split('').splice(0, 4).join('')
	const getCosts = cost => (cost / 1000000).toFixed(2) + 'm'
	return (
		<div className='Ad'>
			<div className='image'></div>
			<div className='details'>
				<h2>Apartment Details:</h2>
				<p>Id: {fileNumber}</p>
				<p>Location: {location}</p>
				<p>Area: {area}&#13217;</p>
				<p>Year of Construction: {getBuildDate(buildDate)}</p>
				<p>Cost Per Meter: {getCosts(costPerMeter)}</p>
				<p>Total Cost: {getCosts(totalCost)}</p>
			</div>
		</div>
	)
}

Ad.defaultProps = {
	fileNumber: 99999,
	location: 'گوهردشت',
	area: 200,
	buildDate: '2020-01-01T00:00:00+00:00',
	costPerMeter: 5000000,
	totalCost: 1000000000
}

export default Ad
