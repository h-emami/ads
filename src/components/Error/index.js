import React from 'react'
import './styles.css'

const Error = ({ error }) => {
	return (
		<div className='Error'>
			<h2>Network Error: {error}</h2>
		</div>
	)
}

export default Error
