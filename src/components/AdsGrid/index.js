import React from 'react'
import './styles.css'
import Ad from '../Ad'
import { connect } from 'react-redux'

const AdsGrid = ({ ads }) => {
	return (
		<div className='AdsGrid'>
			<div className='wrap'>
				{ads.length > 0 &&
					ads.map(
						({
							fileNumber,
							branch,
							area,
							buildDate,
							pricePerSquareMeter,
							totalPrice
						}) => (
							<Ad
								key={fileNumber}
								fileNumber={fileNumber}
								location={branch.addressText}
								area={area}
								buildDate={buildDate}
								costPerMeter={pricePerSquareMeter}
								totalCost={totalPrice}
							/>
						)
					)}
			</div>
		</div>
	)
}

const mapStateToProps = ({ ads }) => ({
	ads: ads.result
})

export default connect(mapStateToProps)(AdsGrid)
