import React from 'react'
import './styles.css'
import { connect } from 'react-redux'

const Header = ({ userName }) => {
	return (
		<div className='Header'>
			<h3>Welcome {userName || 'stranger'}!</h3>
		</div>
	)
}

const mapStateToProps = ({ user }) => ({
	userName: user.userName
})

export default connect(mapStateToProps)(Header)
