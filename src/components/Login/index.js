import React, { useState } from 'react'
import './styles.css'
import { withRouter } from 'react-router-dom'
import { addUserName } from '../../redux/user'
import { connect } from 'react-redux'

const Login = ({ history, addUserName }) => {
	const [userName, setUserName] = useState('')
	const [password, setPassword] = useState('')
	const submitHandler = e => {
		e.preventDefault()
		addUserName(userName)
		history.push('/ads')
	}
	return (
		<div className='Login'>
			<h2>Welcome to Asanshahr Online Real Estate</h2>
			<form onSubmit={submitHandler} className='form'>
				<label htmlFor='username'>Username: </label>
				<input
					type='text'
					id='userName'
					name='userName'
					value={userName}
					minLength='3'
					maxLength='32'
					onChange={e => setUserName(e.target.value)}
					required
				/>
				<label htmlFor='password'>Password: </label>
				<input
					type='password'
					id='password'
					name='password'
					value={password}
					minLength='6'
					maxLength='64'
					onChange={e => setPassword(e.target.value)}
					required
				/>
				<button className='btn' type='submit'>
					Login
				</button>
			</form>
		</div>
	)
}

const mapDispatchToProps = dispatch => ({
	addUserName: userName => dispatch(addUserName(userName))
})

export default withRouter(connect(null, mapDispatchToProps)(Login))
