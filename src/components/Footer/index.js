import React from 'react'
import './styles.css'

const Footer = () => {
	const year = new Date().getFullYear()
	return (
		<div className='Footer'>
			<h2>Asanshahr Online Real Estate</h2>
			<h3>All rights reserved &copy; {year}</h3>
		</div>
	)
}

export default Footer
