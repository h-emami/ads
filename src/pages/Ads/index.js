import React, { useEffect } from 'react'
import AdsGrid from '../../components/AdsGrid'
import Layout from '../../components/Layout'
import { fetchAds } from '../../redux/ads'
import { connect } from 'react-redux'

const Ads = ({ onFetchAds }) => {
	useEffect(() => {
		onFetchAds('https://adsapi.herokuapp.com/')
	}, [])
	return (
		<Layout>
			<AdsGrid />
		</Layout>
	)
}

const mapDispatchToProps = dispatch => ({
	onFetchAds: url => dispatch(fetchAds(url))
})

export default connect(null, mapDispatchToProps)(Ads)
