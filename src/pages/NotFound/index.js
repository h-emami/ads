import React from 'react'
import './styles.css'

const NotFound = () => <div className='NotFound'>404 - Page Not Found!</div>

export default NotFound
