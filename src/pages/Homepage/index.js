import React from 'react'
import './styles.css'
import Login from '../../components/Login'

const Homepage = props => {
	return (
		<div className='Homepage'>
			<Login />
		</div>
	)
}

export default Homepage
