import React, { Suspense } from 'react'
import { Switch, Route } from 'react-router-dom'
import Loading from './components/Loading'
import Homepage from './pages/Homepage'
import NotFound from './pages/NotFound'
import Ads from './pages/Ads'

const App = () => {
	return (
		<Suspense fallback={Loading}>
			<Switch>
				<Route path='/' exact component={Homepage} />
				<Route path='/ads' component={Ads} />
				<Route component={NotFound} />
			</Switch>
		</Suspense>
	)
}

export default App
