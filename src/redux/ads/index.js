// Action Types
const FETCH_ADS_START = 'FETCH_ADS_START'
const FETCH_ADS_SUCCESS = 'FETCH_ADS_SUCCESS'
const FETCH_ADS_FAIL = 'FETCH_ADS_FAIL'

// Action Creators
const fetchAdsStarted = () => ({
	type: FETCH_ADS_START
})

const fetchAdsSuccessful = data => ({
	type: FETCH_ADS_SUCCESS,
	payload: data
})

const fetchAdsFailed = error => ({
	type: FETCH_ADS_FAIL,
	payload: error
})

// Thunk
export const fetchAds = url => async dispatch => {
	dispatch(fetchAdsStarted())
	try {
		const result = await fetch(url)
		const data = await result.json()
		dispatch(fetchAdsSuccessful(data.result))
	} catch (error) {
		dispatch(fetchAdsFailed(error.message))
	}
}

// Reducer
const initialState = {
	result: [],
	isLoading: false,
	error: null
}

const adsReducer = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_ADS_START:
			return {
				...state,
				isLoading: true
			}
		case FETCH_ADS_SUCCESS:
			return {
				...state,
				result: action.payload,
				isLoading: false
			}
		case FETCH_ADS_FAIL:
			return {
				...state,
				isLoading: false,
				error: action.payload
			}
		default:
			return state
	}
}

export default adsReducer
