// Action Types
const ADD_USERNAME = 'ADD_USERNAME'

// Action Creators
export const addUserName = userName => ({
	type: ADD_USERNAME,
	payload: userName
})

// Reducer
const initialState = {
	userName: ''
}

const userReducer = (state = initialState, action) => {
	switch (action.type) {
		case ADD_USERNAME:
			return {
				...state,
				userName: action.payload
			}
		default:
			return state
	}
}

export default userReducer
