import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import rootReducer from '../rootReducer'
import logger from 'redux-logger'

const isDev = process.env.NODE_ENV === 'development'

const middlewares = [isDev && logger, thunk]

export const store = createStore(rootReducer, applyMiddleware(...middlewares))
