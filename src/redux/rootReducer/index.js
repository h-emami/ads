import { combineReducers } from 'redux'
import userReducer from '../user'
import adsReducer from '../ads'

export default combineReducers({
	user: userReducer,
	ads: adsReducer
})
